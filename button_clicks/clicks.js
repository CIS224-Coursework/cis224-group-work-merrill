

function clickCounter() {
    if(typeof(Storage) !== "undefined") {
        
        if (localStorage.clickcount) {
            localStorage.clickcount = Number(localStorage.clickcount)+1;
            
        } else {
            localStorage.clickcount = 1;
        }
        document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s).";
        
        reset();
    }
}
    
var startTime = Date.now();

var interval = setInterval(function() {
    var elapsedTime = Date.now() - startTime;
    document.getElementById("date").innerHTML = "Button last clicked: " + (elapsedTime / 1000).toFixed(2);
    }, 100);
    
function reset(){
    clearInterval(interval);
}

