function makeArrayConsecutive2(statues) {
     var numberArray = statues.length
     var maximumNumber = Math.max(...statues);
     var minimumNumber = Math.min(...statues);
     var rangeOfArray = maximumNumber - minimumNumber + 1;
     var missingStatues = rangeOfArray - numberArray;
     return missingStatues;
}